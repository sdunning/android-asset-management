package gis.apsu.Asset_Management;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dunnings on 12/18/13.
 */
public class LIAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] names, values;

    public LIAdapter(Context context, String[] names, String[] values) {
        super(context, R.layout.my_list_layout, values);
        this.context = context;
        this.names = names;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.my_list_layout, parent, false);
        TextView leftTV = (TextView) rowView.findViewById(R.id.myListLayoutTV);
        TextView rightTV = (TextView) rowView.findViewById(R.id.myListLayoutTV2);
        leftTV.setText(names[position]);
        rightTV.setText(values[position]);



        return rowView;
    }

}