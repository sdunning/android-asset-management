package gis.apsu.Asset_Management;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by dunnings on 12/2/13.
 */
public class ManualSearch extends Activity implements View.OnClickListener {

    private Button manualSearchButton;
    private EditText manualSearchET;
    private String searchResult, caseStr;
    private String[] values = new String[23];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manual_search);

        Intent caseIntent = getIntent();
        caseStr = caseIntent.getStringExtra("Case");

        manualSearchButton = (Button) findViewById(R.id.manualSearchButton);
        manualSearchButton.setOnClickListener(this);

        manualSearchET = (EditText) findViewById(R.id.manualSearchET);

    }

    @Override
    public void onClick(View v) {

        searchResult = manualSearchET.getText().toString();

        if(v.getId() == R.id.manualSearchButton){
            if(caseStr.equals("test")){
                Intent intent = new Intent(this, Results.class);
                intent.putExtra("Result", searchResult);
                intent.putExtra("Values", values);
                startActivity(intent);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(this, EditResults.class);
                intent.putExtra("Result", searchResult);
                intent.putExtra("Values", values);
                startActivity(intent);
                finish();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.homeMenu:
                Intent homeIntent = new Intent(this, MyActivity.class);
                startActivity(homeIntent);
                finish();
                return true;

            case R.id.cancel:
                return true;

            case R.id.about:
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(caseStr.equals("test")){
            Intent intent = new Intent(this, Test.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, Inventory.class);
            startActivity(intent);
        }

        finish();
    }
}
