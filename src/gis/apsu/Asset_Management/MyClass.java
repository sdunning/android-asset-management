package gis.apsu.Asset_Management;

/**
 * Created by dunnings on 12/4/13.
 */
public class MyClass {

    private int fascCode;
    private String fascDesc;
    private int fascClass;

    public MyClass(int fascCode, String fascDesc, int fascClass) {
        this.fascCode = fascCode;
        this.fascDesc = fascDesc;
        this.fascClass = fascClass;
    }

    public MyClass() {
    }

    @Override
    public String toString() {
        return fascDesc;
    }

    public int getFascCode() {
        return fascCode;
    }

    public void setFascCode(int fascCode) {
        this.fascCode = fascCode;
    }

    public String getFascDesc() {
        return fascDesc;
    }

    public void setFascDesc(String fascDesc) {
        this.fascDesc = fascDesc;
    }

    public int getFascClass() {
        return fascClass;
    }

    public void setFascClass(int fascClass) {
        this.fascClass = fascClass;
    }
}

