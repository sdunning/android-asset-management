package gis.apsu.Asset_Management;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by dunnings on 12/20/13.
 */
public class EditLIAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] names, values;

    public EditLIAdapter(Context context, String[] names, String[] values) {
        super(context, R.layout.my_list_layout, values);
        this.context = context;
        this.names = names;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.my_list_layout, parent, false);
        TextView leftTV = (TextView) rowView.findViewById(R.id.myListLayoutTV);
        TextView rightTV = (TextView) rowView.findViewById(R.id.myListLayoutTV2);
        TextView endTV = (TextView) rowView.findViewById(R.id.myListLayoutTV3);
        if(position == 0 || position == 1 || position == 2 || position == 6 || position == 13 || position == 14 || position == 15 || position == 16 || position == 17 || position == 20){

        } else {
            leftTV.setTextColor(context.getResources().getColor(R.color.black));
            rightTV.setTextColor(context.getResources().getColor(R.color.mediumBlue));
        }
        leftTV.setText(names[position]);
        rightTV.setText(values[position]);
        endTV.setText(" >");



        return rowView;
    }

}