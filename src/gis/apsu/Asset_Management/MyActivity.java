package gis.apsu.Asset_Management;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MyActivity extends Activity implements View.OnClickListener{

    private Button testButton, inventoryButton, logOutButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        testButton = (Button) findViewById(R.id.testButton);
        testButton.setOnClickListener(this);

        inventoryButton = (Button) findViewById(R.id.inventoryButton);
        inventoryButton.setOnClickListener(this);

        logOutButton = (Button) findViewById(R.id.logOutButton);
        logOutButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.testButton){

            Intent intent = new Intent(this, Test.class);
            startActivity(intent);
            finish();

        }

        if(v.getId() == R.id.inventoryButton){

            Intent intent = new Intent(this, Inventory.class);
            startActivity(intent);
            finish();

        }

        if(v.getId() == R.id.logOutButton){

            AlertDialog.Builder ad = new AlertDialog.Builder(MyActivity.this);
            ad.setTitle("Log Out");
            ad.setMessage("Confirm Log Out?");
            ad.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MyActivity.this, Login.class);
                    startActivity(intent);
                    finish();
                }
            });
            ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            ad.show();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        menu.removeItem(R.id.homeMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.cancel:
                return true;

            case R.id.about:
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
     public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();
    }


}
