package gis.apsu.Asset_Management;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dunnings on 11/27/13.
 */
public class Results extends ListActivity {

    private String[] names, values;
    public static String ASSETURL;
    private static String XAPIKEY = "";
    private static final String STATUS = "status";
    private String scClass, scCode, dpCode, lCode, lMemo, sCode, cCode, serialCode, mCode, model, modelYear, license, acqDate, acqCost, quantity, lastInvDate, insCost, insCarrier, insValue, insExpDate, insMemo, result;
    private ProgressDialog m_ProgressDialog = null;
    private DbDataSource datasource;
    Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results);

        names = new String[] {
                "Asset ID", "Class", "Subclass", "Department", "Location ID", "Location Memo", "Status",
                "Condition", "Serial ID", "Manufacturer", "Model", "Model Year", "License", "Aquisition Date",
                "Aquisition Cost", "Quantity", "Last Inventory Date", "Insured", "Insurance Carrier",
                "Insurance Value", "Insurance Exp Date", "Insurance Cost", "Insurance Memo"
        };
        values = new String[names.length];

        Intent intent = getIntent();
        final String assetID = intent.getStringExtra("Result");
        result = assetID;

        GetValues runner = new GetValues();
        String sleepTime = String.valueOf(1000);
        runner.execute(sleepTime);

        m_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Test.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.homeMenu:
                Intent homeIntent = new Intent(this, MyActivity.class);
                startActivity(homeIntent);
                finish();
                return true;

            case R.id.cancel:
                return true;

            case R.id.about:
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void getAssetCodes(){

        datasource = new DbDataSource(getApplicationContext());
        datasource.open();

        ASSETURL = this.getResources().getString(R.string.base_url) + this.getResources().getString(R.string.asset_url) + result;
        XAPIKEY = this.getResources().getString(R.string.api_key);

        try{

            JSONObject assetJSON = JSONParser.getJSONFromUrl(ASSETURL, XAPIKEY);
            //Log.i("*** ASSET JSON OBJECT ***", assetJSON.toString());
            JSONObject status = null;
            String success = null;

            try {
                // Getting Array of Login Validation
                status = assetJSON.getJSONObject(STATUS);
                success = status.getString("success");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(success.equals("Matches for query")){
                //Log.i("*** ASSET JSON ***", success);
                JSONArray dataArray = assetJSON.getJSONArray("data");
                //Log.i("**** DATAARRAY ****", dataArray.toString());
                JSONObject object = dataArray.getJSONObject(0);
                scClass = object.get("fama_class").toString();
                scCode = object.get("fama_subcl").toString();
                dpCode = object.get("fama_dept").toString();
                lCode = object.get("fama_loc").toString();
                lMemo = object.get("fama_loc_memo").toString();
                sCode = object.get("fama_status").toString();
                cCode = object.get("fama_cond_cd").toString();
                serialCode = object.get("fama_serial").toString();
                mCode = object.get("fama_manuf").toString();
                model = object.get("fama_model").toString();
                modelYear = object.get("fama_model_yr").toString();
                license = object.get("fama_license").toString();
                acqDate = object.get("fama_acq_dt").toString();
                acqCost = object.get("fama_pur_cost").toString();
                quantity = object.get("fama_qty").toString();
                lastInvDate = object.get("fama_lst_inv_dt").toString();
                insCost = object.get("fama_ins_cost").toString();
                insCarrier = object.get("fama_ins_car").toString();
                insValue = object.get("fama_ins_val").toString();
                insExpDate = object.get("fama_ins_exp_dt").toString();
                insMemo = object.getString("fama_ins_mem1").toString();
                //Log.i("**** CODES ****", "SCCLASS: " + scClass + ", SCCODE: " + scCode + ", DPCODE: " + dpCode + ", LCODE: " + lCode + ", LMEMO: " + lMemo + ", SCODE: " + sCode + ", CCODE: " + cCode + ", MCODE: " + mCode);


            } else {
                Log.i("*** ASSET JSON ***", "THIS NO WORKY!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        datasource.close();



    }

    private Runnable returnRes = new Runnable() {

        @Override
        public void run() {
            m_ProgressDialog.dismiss();
        }
    };

    public void getAssetInfo(){

        values[0] = result;

        if(scClass == null || scClass.equals("null")){
            values[1] = " ";
            values[2] = " ";
        } else {
            //Log.i("*** SCCLASS ***", scClass);
            MyClass myClass = datasource.getClassInfo(scClass, scCode);

            if(myClass.getFascClass() == 50){
                values[1] = String.valueOf(myClass.getFascDesc());
                values[2] = " ";
            } else {
                MyClass subClass = datasource.getSubclassInfo(scClass, scCode);
                values[2] = subClass.getFascDesc();
                //Log.i("*** SUBCLASS CLASS VALUE ***", String.valueOf(subClass.getFascClass()));
                MyClass subClass2 = datasource.getClassInfo(String.valueOf(subClass.getFascClass()), String.valueOf(subClass.getFascCode()));
                values[1] = String.valueOf(subClass2.getFascDesc());
            }

        }

        if(dpCode.equals("null") || dpCode.equals(null)){
            values[3] = " ";
        } else {
            MyDept myDept = datasource.getDeptInfo(dpCode);
            values[3] = myDept.getFadpDesc();
        }

        if(lCode.equals("null") || lCode.equals(null) || lCode.equals("")){
            values[4] = " ";
        } else {
            MyLoc myLoc = datasource.getLocInfo(lCode);
            values[4] = myLoc.getLocDesc();
        }

        values[5] = lMemo;

        if(sCode.equals("null") || sCode.equals(null)){
            values[6] = " ";
        } else {
            MyStatus myStatus = datasource.getStatusInfo(sCode);
            values[6] = myStatus.getStatusDesc();
        }

        if(cCode.equals("null") || cCode.equals(null) || cCode.equals("")){
            values[7] = " ";
        } else {
            MyCond myCond = datasource.getCondInfo(cCode);
            values[7] = myCond.getCondDesc();
        }

        if(serialCode.equals("null")){
            values[8] = " ";
        } else {
            values[8] = serialCode;
        }

        if(mCode.equals("null") || mCode.equals(null) || mCode.equals("")){
            values[9] = " ";
        } else {
            MyManufac myManufac = datasource.getManufacInfo(mCode);
            values[9] = myManufac.getManufacDesc();
        }

        if(model.equals("null")){
            values[10] = " ";
        } else {
            values[10] = model;
        }
        if(modelYear.equals("null") || modelYear.equals(null) || modelYear.equals("") || modelYear.equals("0")){
            values[11] = "0";
        } else {
            values[11] = modelYear;
        }
        if(license.equals("null")){
            values[12] = " ";
        } else {
            values[12] = license;
        }
        if(acqDate.equals("null")){
            values[13] = " ";
        } else {
            String[] acqDateArr = acqDate.split(" ");
            values[13] = acqDateArr[0];
        }
        if(acqCost.equals("null")){
            values[14] = " ";
        } else {
            values[14] = acqCost;
        }
        if(quantity.equals("null")){
            values[15] = " ";
        } else {
            values[15] = quantity;
        }
        if(lastInvDate.equals("null")){
            values[16] = " ";
        } else {
            String[] lastInvDateArr = lastInvDate.split(" ");
            values[16] = lastInvDateArr[0];
        }

        if(insCost.equals("0.00") || insCost.equals("null")){
            values[17] = "NO";
            insCost = "0";
        } else {
            values[17] = "YES";
        }

        if(insCarrier.equals("null")){
            values[8] = " ";
        } else {
            values[18] = insCarrier;
        }

        if(insValue.equals("0.00") || insValue.equals("null")){
            insValue = "0";
        }
        values[19] = insValue;

        String[] insExpDateArr = insExpDate.split(" ");
        if(values[17].equals("NO")){
            values[20] = " ";
        } else {
            values[20] = insExpDateArr[0];
        }
        values[21] = insCost;

        if(insMemo.equals("null")){
            values[22] = " ";
        } else {
            values[22] = insMemo;
        }

    }

    public class GetValues extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            publishProgress("Working....");

            try{
                getAssetCodes();
                getAssetInfo();
            } catch (Exception e){
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            setListAdapter(new LIAdapter(Results.this, names, values));
            runOnUiThread(returnRes);

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

}
