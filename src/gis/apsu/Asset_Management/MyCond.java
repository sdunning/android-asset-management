package gis.apsu.Asset_Management;

/**
 * Created by dunnings on 12/12/13.
 */
public class MyCond {

    private String condCode;
    private String condDesc;

    public MyCond(String condCode, String condDesc) {
        this.condCode = condCode;
        this.condDesc = condDesc;
    }
    public MyCond(){
    }
    @Override
    public String toString() {
        return condDesc;
    }

    public String getCondCode() {
        return condCode;
    }

    public void setCondCode(String condCode) {
        this.condCode = condCode;
    }

    public String getCondDesc() {
        return condDesc;
    }

    public void setCondDesc(String condDesc) {
        this.condDesc = condDesc;
    }
}
