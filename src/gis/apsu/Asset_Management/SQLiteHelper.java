package gis.apsu.Asset_Management;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dunnings on 12/3/13.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "AMDatabase.db";
    private static final int DATABASE_VERSION = 1;
    private static SQLiteDatabase database;

    //class table
    public static final String CLASS_TABLE = "class";
    public static final String FASC_CLASS = "fasc_class";
    public static final String FASC_CODE = "fasc_code";
    public static final String FASC_DESC = "fasc_desc";

    //department table
    public static final String DEPARTMENT_TABLE = "department";
    public static final String FADP_CODE = "fadp_code";
    public static final String FADP_DESC = "fadp_desc";

    //location table
    public static final String LOCATION_TABLE = "location";
    public static final String LOC_CODE = "loc_code";
    public static final String LOC_DESC = "loc_desc";

    //status table
    public static final String STATUS_TABLE = "status";
    public static final String STATUS_CODE = "status_code";
    public static final String STATUS_DESC = "status_desc";

    //condition table
    public static final String CONDITION_TABLE = "ccondition";
    public static final String COND_CODE = "cond_code";
    public static final String COND_DESC = "cond_desc";

    //manufacturer table
    public static final String MANUFACTURER_TABLE = "manufacturer";
    public static final String FAIM_CODE = "faim_code";
    public static final String FAIM_NAME = "faim_name";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String classSql = "CREATE TABLE " + CLASS_TABLE + " ("
                + FASC_CODE + " integer not null,"
                + FASC_DESC + " text not null,"
                + FASC_CLASS + " integer not null"
                + ")";

        String departmentSql = "CREATE TABLE " + DEPARTMENT_TABLE + " ("
                + FADP_DESC + " text not null,"
                + FADP_CODE + " integer not null"
                + ")";

        String locationSql = "CREATE TABLE " + LOCATION_TABLE + " ("
                + LOC_CODE + " integer not null,"
                + LOC_DESC + " text not null"
                + ")";

        String statusSql = "CREATE TABLE " + STATUS_TABLE + " ("
                + STATUS_CODE + " text not null,"
                + STATUS_DESC + " text not null"
                + ")";

        String conditionSql = "CREATE TABLE " + CONDITION_TABLE + " ("
                + COND_DESC + " text not null,"
                + COND_CODE + " text not null"
                + ")";

        String manufacturerSql = "CREATE TABLE " + MANUFACTURER_TABLE + " ("
                + FAIM_CODE + " text not null,"
                + FAIM_NAME + " text not null"
                + ")";

        /*Log.i("CREATED CLASS", classSql);
        Log.i("CREATED DEPARTMENT", departmentSql);
        Log.i("CREATED LOCATION", locationSql);
        Log.i("CREATED STATUS", statusSql);
        Log.i("CREATED CONDITION", conditionSql);
        Log.i("CREATED MANUFACTURER", manufacturerSql);*/

        db.execSQL(classSql);
        db.execSQL(departmentSql);
        db.execSQL(locationSql);
        db.execSQL(statusSql);
        db.execSQL(conditionSql);
        db.execSQL(manufacturerSql);

    }

    public void deleteDatabase(){
        this.deleteDatabase();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
