package gis.apsu.Asset_Management;

/**
 * Created by dunnings on 12/12/13.
 */
public class MyLoc {

    private int locCode;
    private String locDesc;

    public MyLoc() {
    }

    @Override
    public String toString() {
        return locDesc;
    }

    public int getLocCode() {
        return locCode;
    }

    public void setLocCode(int fadpCode) {
        this.locCode = fadpCode;
    }

    public String getLocDesc() {
        return locDesc;
    }

    public void setLocDesc(String locDesc) {
        this.locDesc = locDesc;
    }

    public MyLoc(int locCode, String locDesc) {
        this.locCode = locCode;
        this.locDesc = locDesc;
    }



}
