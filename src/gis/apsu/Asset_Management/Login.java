package gis.apsu.Asset_Management;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by dunnings on 11/22/13.
 */
public class Login extends Activity {

    private Button signInButton;
    private static String LOGINURL;
    private static String TABLESURL;
    private static String TAG = "";
    // Number of Days to keep preferences stored
    private static final int TIMEOUT = 0;
    private static String XAPIKEY = "";
    // JSon Array Nodes
    private static final String VALID = "login";
    private static final String STATUS = "status";
    private static final String API = "api";
    private static final String USER = "user";
    private String userName = null;
    private int id = 0;
    String loginmessage = null;
    Thread t;
    Thread t2;
    private SecurePreferences mPreferences;
    private SharedPreferences DBMadePrefs;
    private static final String DBMadePrefsName = "DBMadePrefsFile";
    private Boolean DBMade = false;
    private SharedPreferences.Editor editor;
    ProgressDialog dialog;
    private DbDataSource datasource;
    private String[] code, desc, fClass, dcode, ddesc, lcode, ldesc, scode, sdesc, ccode, cdesc, mcode, name;
    private ProgressDialog m_ProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        clearDatabase();

        LOGINURL = this.getResources().getString(R.string.base_url) + this.getResources().getString(R.string.login_url);
        TABLESURL = this.getResources().getString(R.string.base_url) + this.getResources().getString(R.string.tables_url);
        XAPIKEY = this.getResources().getString(R.string.api_key);
        TAG = this.getResources().getString(R.string.login);

        mPreferences = new SecurePreferences(this);

        DBMadePrefs = getSharedPreferences(DBMadePrefsName, 0);
        editor = DBMadePrefs.edit();
        editor.putBoolean("DBMade", DBMade);
        editor.commit();


        if (!checkLoginInfo()) {
            signInButton = (Button) findViewById(R.id.signInButton);
            signInButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    showDialog(0);
                    t = new Thread() {
                        public void run() {
                            tryLogin();
                        }
                    };
                    t.start();

                }
            });
        } else {
			/*
			 * Directly opens the Welcome page, if the username and password is
			 * already available in the SharedPreferences
			 */

            Intent intent = new Intent(getApplicationContext(), MyActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0: {
                dialog = new ProgressDialog(this);
                dialog.setMessage("Signing In, Please Wait...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            }
        }
        return null;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String loginmsg = (String) msg.obj;
            if (loginmsg.equals("SUCCESS")) {
                removeDialog(0);

                Intent intent = new Intent(getApplicationContext(), MyActivity.class);
                startActivity(intent);
                finish();
            }
        }
    };

    public void tryLogin() {
        // contacts JSONArray
        JSONObject login = null;
        JSONObject status = null;
        JSONObject api = null;
        JSONObject user = null;
        boolean valid = false;
        String apikey = null, success = null;

        Log.v(TAG, "Trying to Login");
        EditText etxt_user = (EditText) findViewById(R.id.usernameET);
        EditText etxt_pass = (EditText) findViewById(R.id.passwordET);
        String username = etxt_user.getText().toString();
        String password = etxt_pass.getText().toString();

        List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
        pairs.add(new BasicNameValuePair("username", username));
        pairs.add(new BasicNameValuePair("password", password));

        try {
            // Creating JSON Parser instance
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = JSONParser.getJSONFromUrl(LOGINURL, XAPIKEY,
                    pairs);

            try {
                // Getting Array of Login Validation
                login = json.getJSONObject(VALID);
                valid = login.getBoolean("valid");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (valid) {

                Date dateNow = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                StringBuilder nowMMDDYYYY = new StringBuilder(
                        sdf.format(dateNow));
                String dateInString = nowMMDDYYYY.toString();
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(dateInString));
                } catch (ParseException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                c.add(Calendar.DATE, TIMEOUT);
                Date resultdate = new Date(c.getTimeInMillis());
                dateInString = sdf.format(resultdate);
                System.out.println("String date:" + dateInString);

                // get new api key
                try {
                    // Getting Array of Login Validation
                    api = json.getJSONObject(API);
                    apikey = api.getString("key");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Store the username and password in SharedPreferences after
                // the successful login
                SecurePreferences.Editor editor = mPreferences.edit();
                editor.putString("username", username);
                editor.putString("password", password);
                editor.putString("logdate", dateInString);
                editor.putString("apikey", apikey);
                /*editor.putString("first", first);
                editor.putString("last", last);*/
                editor.commit();

                //Add the Lookup Tables to database
                /*t2 = new Thread() {
                    public void run() {
                        getLookUps();
                    }
                };
                t2.start();*/
                getLookUps();


                Message myMessage = new Message();
                myMessage.obj = "SUCCESS";
                handler.sendMessage(myMessage);

            } else if (!valid) {
                Intent intent = new Intent(getApplicationContext(),
                        LoginError.class);
                intent.putExtra("LoginMessage", "Login Failure!");
                startActivity(intent);
                removeDialog(0);
            }
        } catch (Exception e) {
            Intent intent = new Intent(getApplicationContext(),
                    LoginError.class);
            intent.putExtra("LoginMessage", "Unable to login");
            startActivity(intent);
            removeDialog(0);
        }
    }

    private final boolean checkLoginInfo() {
        boolean ysnCheck = false;
        String strInfo;
        Date dayDate;
        boolean username_set = mPreferences.contains("username");
        boolean password_set = mPreferences.contains("password");
        if (username_set || password_set) {
            ysnCheck = true;
        }

        if (ysnCheck) {
            strInfo = mPreferences.getString("logdate", null);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                dayDate = sdf.parse(strInfo);
                Date today=new Date();
                ysnCheck=today.before(dayDate);
                if (!ysnCheck){
                    // Store the username and password in SharedPreferences after
                    // the successful login
                    SecurePreferences.Editor editor = mPreferences.edit();
                    editor.remove("username");
                    editor.remove("password");
                    editor.remove("logdate");
                    editor.remove("apikey");
                    editor.commit();
                }
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return ysnCheck;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        menu.removeItem(R.id.homeMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.cancel:
                return true;

            case R.id.about:
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void getLookUps(){

        try{

            JSONObject tableJson = JSONParser.getJSONFromUrl(TABLESURL, XAPIKEY);
            //Log.i("*** TABLES JSON OBJECT ***", tableJson.toString());
            JSONObject status = null;
            String success = null;

            try {
                // Getting Array of Login Validation
                status = tableJson.getJSONObject(STATUS);
                success = status.getString("success");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(success.equals("Table values found")){
                //Log.i("*** TABLES JSON ***", success);
                JSONObject dataObj = tableJson.getJSONObject("data");
                //Log.i("**** DATAOBJ ****", dataObj.toString());
                JSONArray classArray = dataObj.getJSONArray("class");
                //Log.i("**** CLASSARRAY ****", classArray.toString());

                code = new String[classArray.length()];
                desc = new String[classArray.length()];
                fClass = new String[classArray.length()];

                //Class Table
                for(int i = 0; i < classArray.length(); i++){
                    JSONObject element = classArray.getJSONObject(i);
                    //Log.i("**** ELEMENT ****", element.toString());
                    String test = element.toString();
                    //Log.i("**** TEST ****", test);
                    String[] testSplit = test.split(",");
                    String codeStr = testSplit[0].toString();
                    //Log.i("**** TESTSPLIT ****", codeStr);
                    String descStr = testSplit[1].toString();
                    //Log.i("**** TESTSPLIT ****", descStr);
                    String classStr = testSplit[2].toString();
                    //Log.i("**** TESTSPLIT ****", classStr);
                    String[] codeSplitter = codeStr.split(":");
                    String[] descSplitter = descStr.split(":");
                    String[] classSplitter = classStr.split(":");
                    //Log.i("**** CODESPLITTER ****", codeSplitter[1].toString());
                    //Log.i("**** DESCSPLITTER ****", descSplitter[1].toString());
                    //Log.i("**** CLASSSPLITTER ****", classSplitter[1].toString());
                    String[] codeArr = codeSplitter[1].split("\"");
                    String[] descArr = descSplitter[1].split("\"");
                    String[] classArr = classSplitter[1].split("\"");
                    code[i] = String.valueOf(codeArr[1]);
                    //Log.i("**** CODE ****", code[i]);
                    desc[i] = descArr[1];
                    //Log.i("**** DESC ****", desc[i]);
                    fClass[i] = String.valueOf(classArr[1]);
                    //Log.i("**** CLASS ****", fClass[i]);
                }

                JSONArray deptArray = dataObj.getJSONArray("department");
                //Log.i("**** DEPTARRAY ****", deptArray.toString());
                dcode = new String[deptArray.length()];
                ddesc = new String[deptArray.length()];


                //Department Table
                for(int i = 0; i < deptArray.length(); i++){
                    JSONObject element = deptArray.getJSONObject(i);
                    //Log.i("**** ELEMENT ****", element.toString());
                    String test = element.toString();
                    //Log.i("**** TEST ****", test);
                    String[] testSplit = test.split(",");
                    String codeStr = testSplit[1].toString();
                    //Log.i("**** TESTSPLIT ****", codeStr);
                    String descStr = testSplit[0].toString();
                    //Log.i("**** TESTSPLIT ****", descStr);
                    String[] codeSplitter = codeStr.split(":");
                    String[] descSplitter = descStr.split(":");
                    //Log.i("**** CODESPLITTER ****", codeSplitter[1].toString());
                    //Log.i("**** DESCSPLITTER ****", descSplitter[1].toString());
                    String[] codeArr = codeSplitter[1].split("\"");
                    String[] descArr = descSplitter[1].split("\"");
                    dcode[i] = String.valueOf(codeArr[1]);
                    //Log.i("**** DCODE ****", dcode[i]);
                    ddesc[i] = descArr[1];
                    //Log.i("**** DDESC ****", ddesc[i]);
                }

                JSONArray locArray = dataObj.getJSONArray("location");
                //Log.i("**** LOCARRAY ****", locArray.toString());
                lcode = new String[locArray.length()];
                ldesc = new String[locArray.length()];


                //Location Table
                for(int i = 0; i < locArray.length(); i++){
                    JSONObject element = locArray.getJSONObject(i);
                    //Log.i("**** ELEMENT ****", element.toString());
                    String test = element.toString();
                    //Log.i("**** TEST ****", test);
                    String[] testSplit = test.split(",");
                    String codeStr = testSplit[0].toString();
                    //Log.i("**** TESTSPLIT ****", codeStr);
                    String descStr = testSplit[1].toString();
                    //Log.i("**** TESTSPLIT ****", descStr);
                    String[] codeSplitter = codeStr.split(":");
                    String[] descSplitter = descStr.split(":");
                    //Log.i("**** CODESPLITTER ****", codeSplitter[1].toString());
                    //Log.i("**** DESCSPLITTER ****", descSplitter[1].toString());
                    String[] codeArr = codeSplitter[1].split("\"");
                    String[] descArr = descSplitter[1].split("\"");
                    lcode[i] = String.valueOf(codeArr[1]);
                    //Log.i("**** LCODE ****", lcode[i]);
                    ldesc[i] = descArr[1];
                    //Log.i("**** LDESC ****", ldesc[i]);
                }

                JSONArray statusArray = dataObj.getJSONArray("status");
                //Log.i("**** STATUSARRAY ****", statusArray.toString());
                scode = new String[deptArray.length()];
                sdesc = new String[deptArray.length()];


                //Status Table
                for(int i = 0; i < statusArray.length(); i++){
                    JSONObject element = statusArray.getJSONObject(i);
                    //Log.i("**** ELEMENT ****", element.toString());
                    String test = element.toString();
                    //Log.i("**** TEST ****", test);
                    String[] testSplit = test.split(",");
                    String codeStr = testSplit[0].toString();
                    //Log.i("**** TESTSPLIT ****", codeStr);
                    String descStr = testSplit[1].toString();
                    //Log.i("**** TESTSPLIT ****", descStr);
                    String[] codeSplitter = codeStr.split(":");
                    String[] descSplitter = descStr.split(":");
                    //Log.i("**** CODESPLITTER ****", codeSplitter[1].toString());
                    //Log.i("**** DESCSPLITTER ****", descSplitter[1].toString());
                    String[] codeArr = codeSplitter[1].split("\"");
                    String[] descArr = descSplitter[1].split("\"");
                    scode[i] = codeArr[1];
                    //Log.i("**** SCODE ****", scode[i]);
                    sdesc[i] = descArr[1];
                    //Log.i("**** SDESC ****", sdesc[i]);
                }

                JSONArray condArray = dataObj.getJSONArray("condition");
                //Log.i("**** CONDARRAY ****", condArray.toString());
                ccode = new String[condArray.length()];
                cdesc = new String[condArray.length()];


                //Condition Table
                for(int i = 0; i < condArray.length(); i++){
                    JSONObject element = condArray.getJSONObject(i);
                    //Log.i("**** ELEMENT ****", element.toString());
                    String test = element.toString();
                    //Log.i("**** TEST ****", test);
                    String[] testSplit = test.split(",");
                    String codeStr = testSplit[1].toString();
                    //Log.i("**** TESTSPLIT ****", codeStr);
                    String descStr = testSplit[0].toString();
                    //Log.i("**** TESTSPLIT ****", descStr);
                    String[] codeSplitter = codeStr.split(":");
                    String[] descSplitter = descStr.split(":");
                    //Log.i("**** CODESPLITTER ****", codeSplitter[1].toString());
                    //Log.i("**** DESCSPLITTER ****", descSplitter[1].toString());
                    String[] codeArr = codeSplitter[1].split("\"");
                    String[] descArr = descSplitter[1].split("\"");
                    ccode[i] = String.valueOf(codeArr[1]);
                    //Log.i("**** CCODE ****", ccode[i]);
                    cdesc[i] = descArr[1];
                    //Log.i("**** CDESC ****", cdesc[i]);
                }

                JSONArray manufacArray = dataObj.getJSONArray("manufacturer");
                //Log.i("**** MANUFACARRAY ****", manufacArray.toString());
                mcode = new String[manufacArray.length()];
                name = new String[manufacArray.length()];


                //Manufacturer Table
                for(int i = 0; i < manufacArray.length(); i++){
                    JSONObject element = manufacArray.getJSONObject(i);
                    //Log.i("**** ELEMENT ****", element.toString());
                    String test = element.toString();
                    //Log.i("**** TEST ****", test);
                    String[] testSplit = test.split(",");
                    String codeStr = testSplit[0].toString();
                    //Log.i("**** TESTSPLIT ****", codeStr);
                    String descStr = testSplit[1].toString();
                    //Log.i("**** TESTSPLIT ****", descStr);
                    String[] codeSplitter = codeStr.split(":");
                    String[] descSplitter = descStr.split(":");
                    //Log.i("**** CODESPLITTER ****", codeSplitter[1].toString());
                    //Log.i("**** DESCSPLITTER ****", descSplitter[1].toString());
                    String[] codeArr = codeSplitter[1].split("\"");
                    String[] descArr = descSplitter[1].split("\"");
                    mcode[i] = String.valueOf(codeArr[1]);
                    //Log.i("**** MCODE ****", mcode[i]);
                    name[i] = descArr[1];
                    //Log.i("**** MNAME ****", name[i]);
                }

                datasource = new DbDataSource(getApplicationContext());
                datasource.open();

                for(int i = 0; i < classArray.length(); i++){
                    MyClass myClass = datasource.createClass(Integer.parseInt(code[i]), desc[i], Integer.parseInt(fClass[i]));
                }
                datasource.close();

                datasource.open();

                for(int i = 0; i < deptArray.length(); i++){
                    MyDept myDept = datasource.createDept(Integer.parseInt(dcode[i]), ddesc[i]);
                }
                datasource.close();

                datasource.open();

                for(int i = 0; i < locArray.length(); i++){
                    MyLoc myLoc = datasource.createLoc(Integer.parseInt(lcode[i]), ldesc[i]);
                }
                datasource.close();

                datasource.open();

                for(int i = 0; i < statusArray.length(); i++){
                    MyStatus myStatus = datasource.createStatus(scode[i], sdesc[i]);
                }
                datasource.close();

                datasource.open();

                for(int i = 0; i < condArray.length(); i++){
                    MyCond myCond = datasource.createCond(ccode[i], cdesc[i]);
                }
                datasource.close();

                datasource.open();

                for(int i = 0; i < manufacArray.length(); i++){
                    MyManufac myManufac = datasource.createManufac(mcode[i], name[i]);
                }

                //Log.i("** DATABASE", " CREATED **");
                datasource.close();

                DBMade = true;
                editor = DBMadePrefs.edit();
                editor.putBoolean("DBMade", DBMade);
                editor.commit();


            } else {
                Log.i("*** TABLES JSON ***", "THIS NO WORKY!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //runOnUiThread(returnRes);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(DBMadePrefs.getBoolean("DBMade", true)){
            clearDatabase();
        }
        finish();
    }

    private void clearDatabase(){
        this.deleteDatabase("AMDatabase.db");
    }



}
