package gis.apsu.Asset_Management;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by dunnings on 12/4/13.
 */
public class DbDataSource {

    private SQLiteDatabase database;
    private SQLiteHelper databaseHelper;

    private static final String[] CLASS_COLUMNS = {
            SQLiteHelper.FASC_CODE,
            SQLiteHelper.FASC_DESC,
            SQLiteHelper.FASC_CLASS};

    private static final String[] DEPARTMENT_COLUMNS = {
            SQLiteHelper.FADP_DESC,
            SQLiteHelper.FADP_CODE};

    private static final String[] LOCATION_COLUMNS = {
            SQLiteHelper.LOC_CODE,
            SQLiteHelper.LOC_DESC};

    private static final String[] STATUS_COLUMNS = {
            SQLiteHelper.STATUS_CODE,
            SQLiteHelper.STATUS_DESC};

    private static final String[] CONDITION_COLUMNS = {
            SQLiteHelper.COND_DESC,
            SQLiteHelper.COND_CODE};

    private static final String[] MANUFACTURER_COLUMNS = {
            SQLiteHelper.FAIM_CODE,
            SQLiteHelper.FAIM_NAME};

    public DbDataSource(Context context){
        databaseHelper = new SQLiteHelper(context);
    }

    public void open(){
        database = databaseHelper.getReadableDatabase();
    }

    public void close(){
        databaseHelper.close();
    }

    public MyClass createClass(int fascCode, String fascDesc, int fascClass){

        ContentValues values = new ContentValues();

        values.put(SQLiteHelper.FASC_CODE, fascCode);
        values.put(SQLiteHelper.FASC_DESC, fascDesc);
        values.put(SQLiteHelper.FASC_CLASS, fascClass);
        //Log.i("CREATE CLASS", fascDesc);

        int id = (int)database.insert(SQLiteHelper.CLASS_TABLE,
                null, values);

        Cursor cursor = database.query(SQLiteHelper.CLASS_TABLE,
                CLASS_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();
        MyClass jNote = cursorToClass(cursor);
        cursor.close();
        return jNote;
    }

    public MyDept createDept(int fadpCode, String fadpDesc){

        ContentValues values = new ContentValues();

        values.put(SQLiteHelper.FADP_CODE, fadpCode);
        values.put(SQLiteHelper.FADP_DESC, fadpDesc);
        //Log.i("CREATE DEPT", fadpDesc);

        int id = (int)database.insert(SQLiteHelper.DEPARTMENT_TABLE,
                null, values);

        Cursor cursor = database.query(SQLiteHelper.DEPARTMENT_TABLE,
                DEPARTMENT_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();
        MyDept jNote = cursorToDept(cursor);
        cursor.close();
        return jNote;
    }

    public MyLoc createLoc(int locCode, String locDesc){

        ContentValues values = new ContentValues();

        values.put(SQLiteHelper.LOC_CODE, locCode);
        values.put(SQLiteHelper.LOC_DESC, locDesc);
        //Log.i("CREATE LOC", locDesc);

        int id = (int)database.insert(SQLiteHelper.LOCATION_TABLE,
                null, values);

        Cursor cursor = database.query(SQLiteHelper.LOCATION_TABLE,
                LOCATION_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();
        MyLoc jNote = cursorToLoc(cursor);
        cursor.close();
        return jNote;
    }

    public MyStatus createStatus(String statusCode, String statusDesc){

        ContentValues values = new ContentValues();

        values.put(SQLiteHelper.STATUS_CODE, statusCode);
        values.put(SQLiteHelper.STATUS_DESC, statusDesc);
        //Log.i("CREATE STATUS", statusDesc);

        int id = (int)database.insert(SQLiteHelper.STATUS_TABLE,
                null, values);

        Cursor cursor = database.query(SQLiteHelper.STATUS_TABLE,
                STATUS_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();
        MyStatus jNote = cursorToStatus(cursor);
        cursor.close();
        return jNote;
    }

    public MyCond createCond(String condCode, String condDesc){

        ContentValues values = new ContentValues();

        values.put(SQLiteHelper.COND_CODE, condCode);
        values.put(SQLiteHelper.COND_DESC, condDesc);
        //Log.i("CREATE CONDITION", condDesc);

        int id = (int)database.insert(SQLiteHelper.CONDITION_TABLE,
                null, values);

        Cursor cursor = database.query(SQLiteHelper.CONDITION_TABLE,
                CONDITION_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();
        MyCond jNote = cursorToCond(cursor);
        cursor.close();
        return jNote;
    }

    public MyManufac createManufac(String manufacCode, String manufacDesc){

        ContentValues values = new ContentValues();

        values.put(SQLiteHelper.FAIM_CODE, manufacCode);
        values.put(SQLiteHelper.FAIM_NAME, manufacDesc);
        //Log.i("CREATE MANUFAC", manufacDesc);

        int id = (int)database.insert(SQLiteHelper.MANUFACTURER_TABLE,
                null, values);

        Cursor cursor = database.query(SQLiteHelper.MANUFACTURER_TABLE,
                MANUFACTURER_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();
        MyManufac jNote = cursorToManufac(cursor);
        cursor.close();
        return jNote;
    }

    public MyClass getClassInfo(String scClass, String scCode){

        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.CLASS_TABLE + " WHERE " + SQLiteHelper.FASC_CLASS + " =? AND " + SQLiteHelper.FASC_CODE + " =? ", new String[]{scClass, scCode});
        cursor.moveToFirst();
        //Log.i("**** CURSOR LENGTH ****", String.valueOf(cursor.getCount()));

        MyClass myClass = new MyClass(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                    Integer.parseInt(cursor.getString(2)));

        cursor.close();
        close();
        return myClass;
    }

    public MyClass getSubclassInfo(String scClass, String scCode){

        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.CLASS_TABLE + " WHERE " + SQLiteHelper.FASC_CLASS + " =? AND " + SQLiteHelper.FASC_CODE + " =? ", new String[]{scClass, scCode});
        cursor.moveToFirst();

        MyClass myClass = new MyClass(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                Integer.parseInt(cursor.getString(2)));

        cursor.close();
        close();
        return myClass;
    }

    public MyDept getDeptInfo(String dpCode){

        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.DEPARTMENT_TABLE + " WHERE " + SQLiteHelper.FADP_CODE + " =? ", new String[]{dpCode});
        cursor.moveToFirst();

        //Log.i("**** DEPT CURSOR ****", String.valueOf(cursor.getCount()));


        MyDept myDept = new MyDept();
        myDept.setFadpCode(Integer.parseInt(dpCode));
        myDept.setFadpDesc(cursor.getString(0));

        cursor.close();
        close();
        return myDept;
    }

    public MyDept getDeptCode(String dpDesc){
        open();
        String qStr = "SELECT * FROM " + SQLiteHelper.DEPARTMENT_TABLE + " WHERE " + SQLiteHelper.FADP_DESC + " = '" + dpDesc + "'";
        Cursor cursor = database.rawQuery(qStr, null);
        cursor.moveToFirst();

        MyDept myDept = new MyDept();
        myDept.setFadpCode(Integer.parseInt(cursor.getString(1)));
        myDept.setFadpDesc(cursor.getString(0));

        cursor.close();
        close();
        return myDept;
    }

    /*public ArrayList<MyDept> getDepts(){

        open();

        ArrayList<MyDept> depts = new ArrayList<MyDept>();

        String qStr = "SELECT * FROM " + SQLiteHelper.DEPARTMENT_TABLE;
        Cursor cursor = database.rawQuery(qStr, null);

        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {

            MyDept dept = new MyDept();

            dept.setFadpCode(Integer.parseInt(cursor.getString(1)));
            dept.setFadpDesc(cursor.getString(0));

            depts.add(dept);
            cursor.moveToNext();
        }

        cursor.close();
        close();

        return depts;
    }*/

    public String[] getDepts(){

        open();

        String qStr = "SELECT * FROM " + SQLiteHelper.DEPARTMENT_TABLE;
        Cursor cursor = database.rawQuery(qStr, null);

        String[] depts = new String[cursor.getCount()];

        cursor.moveToFirst();
        int count = 0;

        while (cursor.isAfterLast() == false) {

            MyDept dept = new MyDept();

            dept.setFadpCode(Integer.parseInt(cursor.getString(1)));
            dept.setFadpDesc(cursor.getString(0));

            depts[count] = dept.getFadpDesc();
            cursor.moveToNext();
            count++;
        }

        cursor.close();
        close();

        return depts;
    }

    public MyLoc getLocInfo(String lCode){

        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.LOCATION_TABLE + " WHERE " + SQLiteHelper.LOC_CODE + " =? ", new String[]{lCode});
        cursor.moveToFirst();

        MyLoc myLoc = new MyLoc();
        myLoc.setLocCode(Integer.parseInt(lCode));
        if(cursor != null && cursor.getCount() > 0){
            myLoc.setLocDesc(cursor.getString(1));
        } else {
            myLoc.setLocDesc(" ");
        }

        cursor.close();
        close();
        return myLoc;
    }

    public MyLoc getLocCode(String lDesc){
        open();
        String qStr = "SELECT * FROM " + SQLiteHelper.LOCATION_TABLE + " WHERE " + SQLiteHelper.LOC_DESC + " = '" + lDesc + "'";
        Cursor cursor = database.rawQuery(qStr, null);
        cursor.moveToFirst();

        MyLoc myLoc = new MyLoc();
        myLoc.setLocCode(Integer.parseInt(cursor.getString(0)));
        myLoc.setLocDesc(cursor.getString(1));

        cursor.close();
        close();
        return myLoc;
    }

    public String[] getLocs(){

        open();

        String qStr = "SELECT * FROM " + SQLiteHelper.LOCATION_TABLE;
        Cursor cursor = database.rawQuery(qStr, null);

        String[] locs = new String[cursor.getCount()];

        cursor.moveToFirst();
        int count = 0;

        while (cursor.isAfterLast() == false) {

            MyLoc loc = new MyLoc();

            loc.setLocCode(Integer.parseInt(cursor.getString(0)));
            loc.setLocDesc(cursor.getString(1));

            locs[count] = loc.getLocDesc();
            cursor.moveToNext();
            count++;
        }

        cursor.close();
        close();

        return locs;
    }

    public MyStatus getStatusInfo(String sCode){

        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.STATUS_TABLE + " WHERE " + SQLiteHelper.STATUS_CODE + " =? ", new String[]{sCode});
        cursor.moveToFirst();

        //Log.i("**** LOC CURSOR ****", String.valueOf(cursor.getCount()));

        MyStatus myStatus = new MyStatus();
        myStatus.setStatusCode(sCode);
        myStatus.setStatusDesc(cursor.getString(1));


        cursor.close();
        close();
        return myStatus;
    }

    public MyCond getCondInfo(String cCode){

        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.CONDITION_TABLE + " WHERE " + SQLiteHelper.COND_CODE + " =? ", new String[]{cCode});
        cursor.moveToFirst();

        //Log.i("**** LOC CURSOR ****", String.valueOf(cursor.getCount()));

        MyCond myCond = new MyCond();
        myCond.setCondCode(cCode);
        myCond.setCondDesc(cursor.getString(0));


        cursor.close();
        close();
        return myCond;
    }

    public MyCond getCondCode(String cDesc){
        open();
        String qStr = "SELECT * FROM " + SQLiteHelper.CONDITION_TABLE + " WHERE " + SQLiteHelper.COND_DESC + " = '" + cDesc + "'";
        Cursor cursor = database.rawQuery(qStr, null);
        cursor.moveToFirst();

        MyCond myCond = new MyCond();
        myCond.setCondCode(cursor.getString(1));
        myCond.setCondDesc(cursor.getString(0));

        cursor.close();
        close();
        return myCond;
    }

    public String[] getConds(){

        open();

        String qStr = "SELECT * FROM " + SQLiteHelper.CONDITION_TABLE;
        Cursor cursor = database.rawQuery(qStr, null);

        String[] conds = new String[cursor.getCount()];

        cursor.moveToFirst();
        int count = 0;

        while (cursor.isAfterLast() == false) {

            MyCond cond = new MyCond();

            cond.setCondCode(cursor.getString(1));
            cond.setCondDesc(cursor.getString(0));

            conds[count] = cond.getCondDesc();
            cursor.moveToNext();
            count++;
        }

        cursor.close();
        close();

        return conds;
    }

    public MyManufac getManufacInfo(String mCode){

        MyManufac myManufac = new MyManufac();
        if(mCode != " "){
            open();
            Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.MANUFACTURER_TABLE + " WHERE " + SQLiteHelper.FAIM_CODE + " =? ", new String[]{mCode});
            cursor.moveToFirst();

            //Log.i("**** LOC CURSOR ****", String.valueOf(cursor.getCount()));

            myManufac = new MyManufac();
            myManufac.setManufacCode(mCode);
            if(cursor != null && cursor.getCount() > 0){
                myManufac.setManufacDesc(cursor.getString(1));
            } else {
                myManufac.setManufacDesc(" ");
            }


            cursor.close();
            close();
        } else {
            myManufac.setManufacDesc(" ");
        }

        return myManufac;
    }

    public MyManufac getManufacCode(String mDesc){
        open();
        String qStr = "SELECT * FROM " + SQLiteHelper.MANUFACTURER_TABLE + " WHERE " + SQLiteHelper.FAIM_NAME + " = '" + mDesc + "'";
        Cursor cursor = database.rawQuery(qStr, null);
        cursor.moveToFirst();

        MyManufac myManufac = new MyManufac();
        myManufac.setManufacCode(cursor.getString(0));
        myManufac.setManufacDesc(cursor.getString(1));

        cursor.close();
        close();
        return myManufac;
    }

    public String[] getManufacs(){

        open();

        String qStr = "SELECT * FROM " + SQLiteHelper.MANUFACTURER_TABLE;
        Cursor cursor = database.rawQuery(qStr, null);

        String[] manufacs = new String[cursor.getCount()];

        cursor.moveToFirst();
        int count = 0;

        while (cursor.isAfterLast() == false) {

            MyManufac manufac = new MyManufac();

            manufac.setManufacCode(cursor.getString(0));
            manufac.setManufacDesc(cursor.getString(1));

            manufacs[count] = manufac.getManufacDesc();
            cursor.moveToNext();
            count++;
        }

        cursor.close();
        close();

        return manufacs;
    }

    private MyClass cursorToClass(Cursor cursor){
        MyClass myClass = new MyClass();
        myClass.setFascCode(cursor.getInt(0));
        myClass.setFascDesc(cursor.getString(1));
        myClass.setFascClass(cursor.getInt(2));

        return myClass;
    }
    private MyDept cursorToDept(Cursor cursor){
        MyDept myDept = new MyDept();
        myDept.setFadpCode(cursor.getInt(0));
        myDept.setFadpDesc(cursor.getString(1));

        return myDept;
    }
    private MyLoc cursorToLoc(Cursor cursor){
        MyLoc myLoc = new MyLoc();
        myLoc.setLocCode(cursor.getInt(0));
        myLoc.setLocDesc(cursor.getString(1));

        return myLoc;
    }
    private MyStatus cursorToStatus(Cursor cursor){
        MyStatus myStatus = new MyStatus();
        myStatus.setStatusCode(cursor.getString(0));
        myStatus.setStatusDesc(cursor.getString(1));

        return myStatus;
    }
    private MyCond cursorToCond(Cursor cursor){
        MyCond myCond = new MyCond();
        myCond.setCondCode(cursor.getString(0));
        myCond.setCondDesc(cursor.getString(1));

        return myCond;
    }
    private MyManufac cursorToManufac(Cursor cursor){
        MyManufac myManufac = new MyManufac();
        myManufac.setManufacCode(cursor.getString(0));
        myManufac.setManufacDesc(cursor.getString(1));

        return myManufac;
    }


}
