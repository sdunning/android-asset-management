package gis.apsu.Asset_Management;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by dunnings on 12/20/13.
 */
public class InventoryDetail extends Activity implements View.OnClickListener {

    private Button saveButton, cancelButton;
    private String[] values = new String[22], deptArray = new String[100];
    private String value = "";
    private int pos = 0;
    private EditText inventoryDetailET;
    private Spinner invDetSpinner;
    private DbDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        values = intent.getStringArrayExtra("Values");
        pos = Integer.parseInt(intent.getStringExtra("Position"));
        //Log.i("*** POSITION ***", String.valueOf(pos));
        value = values[pos];

        datasource = new DbDataSource(getApplicationContext());

        if(pos == 3 || pos == 4 || pos == 7 || pos == 9 || pos == 11){
            setContentView(R.layout.inventory_detail_spinner);
            invDetSpinner = (Spinner) findViewById(R.id.invDetSpinner);
            //Log.i("*** SPINNER ***", "List Position was 3, 4, 7, 9, or 11");
            setSpinner();

        } else {
            setContentView(R.layout.inventory_detail);
            inventoryDetailET = (EditText) findViewById(R.id.inventoryDetailET);
            inventoryDetailET.setText(value);
        }
        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(this);

        cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.saveButton){
            if(pos == 3 || pos == 4 || pos == 7 || pos == 9 || pos == 11){
                values[pos] = invDetSpinner.getSelectedItem().toString();
            } else {
                values[pos] = inventoryDetailET.getText().toString();
            }
        }

        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, EditResults.class);
        intent.putExtra("Result", values[0]);
        intent.putExtra("Values", values);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.homeMenu:
                Intent homeIntent = new Intent(this, MyActivity.class);
                startActivity(homeIntent);
                finish();
                return true;

            case R.id.cancel:
                return true;

            case R.id.about:
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void setSpinner(){
        if(pos == 3){

            String[] depts = datasource.getDepts();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, depts);
            adapter.setDropDownViewResource(R.layout.spinner_menu);

            int spinnerPos = adapter.getPosition(value);
            invDetSpinner.setAdapter(adapter);
            invDetSpinner.setSelection(spinnerPos);

        } else if(pos == 4){

            String[] locs = datasource.getLocs();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, locs);
            adapter.setDropDownViewResource(R.layout.spinner_menu);

            int spinnerPos = adapter.getPosition(value);
            invDetSpinner.setAdapter(adapter);
            invDetSpinner.setSelection(spinnerPos);

        } else if(pos == 7){

            String[] conds = datasource.getConds();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, conds);
            adapter.setDropDownViewResource(R.layout.spinner_menu);

            int spinnerPos = adapter.getPosition(value);
            invDetSpinner.setAdapter(adapter);
            invDetSpinner.setSelection(spinnerPos);

        } else if(pos == 9){

            String[] manufacs = datasource.getManufacs();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, manufacs);
            adapter.setDropDownViewResource(R.layout.spinner_menu);

            int spinnerPos = adapter.getPosition(value);
            invDetSpinner.setAdapter(adapter);
            invDetSpinner.setSelection(spinnerPos);

        } else if(pos == 11){

            String[] years = new String[101];

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            String thisYear = sdf.format(c.getTime());

            years[0] = "No Entry";
            for(int i = 1; i < 100; i++){
                int val = Integer.parseInt(thisYear);
                years[i] = String.valueOf(val + 2);
                thisYear = String.valueOf(val - 1);
            }


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, years);
            adapter.setDropDownViewResource(R.layout.spinner_menu);
            int spinnerPos = adapter.getPosition(value);
            invDetSpinner.setAdapter(adapter);
            invDetSpinner.setSelection(spinnerPos);

        }
    }
}
