package gis.apsu.Asset_Management;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by dunnings on 11/22/13.
 */
public class Splash extends Activity {

    private TextView versionTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        versionTV = (TextView) findViewById(R.id.versionTV);

        try{
            String vName = getPackageManager().getPackageInfo(getPackageName(),0).versionName;
            versionTV.setText("v" + vName);
        }catch(Exception e){
            e.printStackTrace();
        }

        Thread splashThread = new Thread(){
            public void run() {
                try{
                    int waited = 0;
                    while(waited < 2000){
                        sleep(100);
                        waited += 100;
                    }
                } catch (InterruptedException e){

                } finally {
                    finish();
                    Intent i = new Intent();
                    i.setClassName("gis.apsu.Asset_Management",
                            "gis.apsu.Asset_Management.Login");
                    startActivity(i);
                }
            }
        };
        splashThread.start();
    }

}