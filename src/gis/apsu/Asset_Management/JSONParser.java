package gis.apsu.Asset_Management;

import android.annotation.SuppressLint;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by dunnings on 11/25/13.
 */
public class JSONParser {

    private static final String TAG = "JSONParser";
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }

    public static JSONObject getJSONFromUrl(String url,String key,List<BasicNameValuePair> pairs) {
        JSONObject jObject = null;

        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        // Depends on your web service
        //httppost.setHeader("Content-type", "application/json");
        httppost.setHeader("X-API-KEY", key);

        InputStream inputStream = null;
        String result = null;
        try {
            Log.i(TAG, "Trying URL " + url);
            Log.i(TAG, "API KEY " + key);
            Log.i(TAG, "PAIRS " + pairs.toString());
            UrlEncodedFormEntity p_entity = new UrlEncodedFormEntity(pairs, HTTP.UTF_8);
            //Log.i("*** FIRST LINE ***", String.valueOf(p_entity));
            httppost.setEntity(p_entity);
            //Log.i("*** SECOND LINE ***", "");
            HttpResponse response = client.execute(httppost);
            Log.v(TAG, response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();

        } catch (Exception e) {
            Log.i(TAG, "Error parsing data " + e.toString());
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (Exception squish) {
            }
        }
        try {
            jObject = new JSONObject(result);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jObject;
    }

    public static JSONObject getJSONFromUrl(String url,String key) {

        JSONObject jObject = null;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(url);
        httpPost.setHeader("X-API-KEY", key);

        InputStream inputStream = null;
        String result = null;
        try {
            Log.i(TAG, "Trying URL " + url);
            Log.i(TAG, "API KEY " + key);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            Log.i(TAG, httpResponse.getStatusLine().toString());


            inputStream = httpEntity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();

        } catch (Exception e) {
            Log.i(TAG, "Error parsing data " + e.toString());
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (Exception squish) {
            }
        }
        try {
            jObject = new JSONObject(result);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jObject;

    }


}