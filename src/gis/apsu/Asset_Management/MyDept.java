package gis.apsu.Asset_Management;

/**
 * Created by dunnings on 12/12/13.
 */
public class MyDept {

    private int fadpCode;
    private String fadpDesc;

    public MyDept(int fadpCode, String fadpDesc) {
        this.fadpCode = fadpCode;
        this.fadpDesc = fadpDesc;
    }

    public MyDept() {
    }

    @Override
    public String toString() {
        return fadpDesc;
    }

    public int getFadpCode() {
        return fadpCode;
    }

    public void setFadpCode(int fadpCode) {
        this.fadpCode = fadpCode;
    }

    public String getFadpDesc() {
        return fadpDesc;
    }

    public void setFadpDesc(String fadpDesc) {
        this.fadpDesc = fadpDesc;
    }



}
