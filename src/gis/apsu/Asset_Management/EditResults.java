package gis.apsu.Asset_Management;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by dunnings on 12/20/13.
 */
public class EditResults extends ListActivity implements View.OnClickListener {

    private String[] values, names;
    public static String ASSETURL, UPDATEURL, timeStr = " ";
    private static String XAPIKEY = "";
    private static final String STATUS = "status";
    private String scClass, scCode, dpCode, lCode, lMemo, sCode, cCode, serialCode, mCode, model, modelYear, license, acqDate, acqCost, quantity, lastInvDate, insCost, insCarrier, insValue, insExpDate, insMemo, result;
    private ProgressDialog m_ProgressDialog = null, cv_ProgressDialog = null;
    private DbDataSource datasource;
    private Button doneButton;
    private boolean doSave = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_results);

        String[] temp = this.getResources().getStringArray(R.array.namesArray);
        names = new String[temp.length];
        names = temp;


        Intent intent = getIntent();
        values = new String[names.length];
        values = intent.getStringArrayExtra("Values");
        final String assetID = intent.getStringExtra("Result");
        result = assetID;

        doneButton = (Button) findViewById(R.id.doneButton);
        doneButton.setOnClickListener(this);

        GetValues runner = new GetValues();
        String sleepTime = String.valueOf(1000);
        runner.execute(sleepTime);

        m_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){

            case R.id.homeMenu:
                Intent homeIntent = new Intent(this, MyActivity.class);
                startActivity(homeIntent);
                finish();
                return true;

            case R.id.cancel:
                return true;

            case R.id.about:
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Inventory.class);
        startActivity(intent);
        finish();
    }

    public void getAssetCodes(){

        datasource = new DbDataSource(getApplicationContext());
        datasource.open();

        ASSETURL = this.getResources().getString(R.string.base_url) + this.getResources().getString(R.string.asset_url) + result;
        XAPIKEY = this.getResources().getString(R.string.api_key);

        try{

            JSONObject assetJSON = JSONParser.getJSONFromUrl(ASSETURL, XAPIKEY);
            //Log.i("*** ASSET JSON OBJECT ***", assetJSON.toString());
            JSONObject status = null;
            String success = null;

            try {
                // Getting Array of Login Validation
                status = assetJSON.getJSONObject(STATUS);
                success = status.getString("success");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(success.equals("Matches for query")){
                //Log.i("*** ASSET JSON ***", success);
                JSONArray dataArray = assetJSON.getJSONArray("data");
                //Log.i("**** DATAARRAY ****", dataArray.toString());
                JSONObject object = dataArray.getJSONObject(0);
                scClass = object.get("fama_class").toString();
                scCode = object.get("fama_subcl").toString();
                dpCode = object.get("fama_dept").toString();
                lCode = object.get("fama_loc").toString();
                lMemo = object.get("fama_loc_memo").toString();
                sCode = object.get("fama_status").toString();
                cCode = object.get("fama_cond_cd").toString();
                serialCode = object.get("fama_serial").toString();
                mCode = object.get("fama_manuf").toString();
                model = object.get("fama_model").toString();
                modelYear = object.get("fama_model_yr").toString();
                license = object.get("fama_license").toString();
                acqDate = object.get("fama_acq_dt").toString();
                acqCost = object.get("fama_pur_cost").toString();
                quantity = object.get("fama_qty").toString();
                lastInvDate = object.get("fama_lst_inv_dt").toString();
                insCost = object.get("fama_ins_cost").toString();
                insCarrier = object.get("fama_ins_car").toString();
                insValue = object.get("fama_ins_val").toString();
                insExpDate = object.get("fama_ins_exp_dt").toString();
                insMemo = object.getString("fama_ins_mem1").toString();



            } else {
                Log.i("*** ASSET JSON ***", "THIS NO WORKY!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        datasource.close();



    }

    private Runnable returnRes = new Runnable() {

        @Override
        public void run() {
            m_ProgressDialog.dismiss();
        }
    };

    public void getAssetInfo(){

        values[0] = result;

        if(scClass == null || scClass.equals("null")){
            values[1] = " ";
            values[2] = " ";
        } else {
            //Log.i("*** SCCLASS ***", scClass);
            MyClass myClass = datasource.getClassInfo(scClass, scCode);

            if(myClass.getFascClass() == 50){
                values[1] = String.valueOf(myClass.getFascDesc());
                values[2] = " ";
            } else {
                MyClass subClass = datasource.getSubclassInfo(scClass, scCode);
                values[2] = subClass.getFascDesc();
                //Log.i("*** SUBCLASS CLASS VALUE ***", String.valueOf(subClass.getFascClass()));
                MyClass subClass2 = datasource.getClassInfo(String.valueOf(subClass.getFascClass()), String.valueOf(subClass.getFascCode()));
                values[1] = String.valueOf(subClass2.getFascDesc());
            }

        }

        if(dpCode.equals("null") || dpCode.equals(null)){
            values[3] = " ";
        } else {
            MyDept myDept = datasource.getDeptInfo(dpCode);
            values[3] = myDept.getFadpDesc();
        }

        if(lCode.equals("null") || lCode.equals(null) || lCode.equals("")){
            values[4] = " ";
        } else {
            MyLoc myLoc = datasource.getLocInfo(lCode);
            values[4] = myLoc.getLocDesc();
        }

        values[5] = lMemo;

        if(sCode.equals("null") || sCode.equals(null)){
            values[6] = " ";
        } else {
            MyStatus myStatus = datasource.getStatusInfo(sCode);
            values[6] = myStatus.getStatusDesc();
        }

        if(cCode.equals("null") || cCode.equals(null) || cCode.equals("")){
            values[7] = " ";
        } else {
            MyCond myCond = datasource.getCondInfo(cCode);
            values[7] = myCond.getCondDesc();
        }

        if(serialCode.equals("null")){
            values[8] = " ";
        } else {
            values[8] = serialCode;
        }

        if(mCode.equals("null") || mCode.equals(null) || mCode.equals("")){
            values[9] = " ";
        } else {
            MyManufac myManufac = datasource.getManufacInfo(mCode);
            values[9] = myManufac.getManufacDesc();
        }

        if(model.equals("null")){
            values[10] = " ";
        } else {
            values[10] = model;
        }
        if(modelYear.equals("null") || modelYear.equals(null) || modelYear.equals("") || modelYear.equals("0") || modelYear.equals("No Entry")){
            values[11] = "0";
        } else {
            values[11] = modelYear;
        }
        if(license.equals("null")){
            values[12] = " ";
        } else {
            values[12] = license;
        }
        if(acqDate.equals("null")){
            values[13] = " ";
        } else {
            String[] acqDateArr = acqDate.split(" ");
            values[13] = acqDateArr[0];
        }
        if(acqCost.equals("null")){
            values[14] = " ";
        } else {
            values[14] = acqCost;
        }
        if(quantity.equals("null")){
            values[15] = " ";
        } else {
            values[15] = quantity;
        }
        if(lastInvDate.equals("null")){
            values[16] = " ";
        } else {
            String[] lastInvDateArr = lastInvDate.split(" ");
            values[16] = lastInvDateArr[0];
        }

        if(insCost.equals("0.00") || insCost.equals("null")){
            values[17] = "NO";
            insCost = "0";
        } else {
            values[17] = "YES";
        }

        if(insCarrier.equals("null")){
            values[8] = " ";
        } else {
            values[18] = insCarrier;
        }

        if(insValue.equals("0.00") || insValue.equals("null")){
            insValue = "0";
        }
        values[19] = insValue;

        String[] insExpDateArr = insExpDate.split(" ");
        if(values[17].equals("NO")){
            values[20] = " ";
        } else {
            values[20] = insExpDateArr[0];
        }
        values[21] = insCost;

        if(insMemo.equals("null")){
            values[22] = " ";
        } else {
            values[22] = insMemo;
        }

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.doneButton){

            AlertDialog.Builder ad = new AlertDialog.Builder(this);
            ad.setTitle("Save");
            ad.setMessage("Save this Inventory to Record?");
            ad.setPositiveButton("Save", new DialogInterface.OnClickListener() {


                @Override
                public void onClick(DialogInterface dialog, int which) {
                    saveRecord();

                }
            });
            ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    onBackPressed();
                }
            });
            ad.show();

        }

    }

    public void saveRecord(){
        ChangeValues cv = new ChangeValues();
        String sleepTime = String.valueOf(1000);
        cv.execute(sleepTime);
        cv_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Sending data ...", true);

        onBackPressed();
    }

    public class GetValues extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            publishProgress("Working....");

            try{
                getAssetCodes();
                if(values[0] == null || values[0].equals("null")){
                    getAssetInfo();
                }
            } catch (Exception e){
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            setListAdapter(new EditLIAdapter(EditResults.this, names, values));
            runOnUiThread(returnRes);

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if(position == 0 || position == 1 || position == 2 || position == 6 || position == 13 || position == 14 || position == 15 || position == 16 || position == 17 || position == 20){

        } else {
            String pos = String.valueOf(position);
            Intent changeIntent = new Intent(this, InventoryDetail.class);
            changeIntent.putExtra("Position", pos);
            changeIntent.putExtra("Values", values);
            startActivity(changeIntent);
            finish();
        }
    }

    public class ChangeValues extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            publishProgress("Working....");

            try{
                getNewCodes();

                changeAssetCodes();
            } catch (Exception e){
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            runOnUiThread(returnCVRes);

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    public void changeAssetCodes(){

        datasource = new DbDataSource(getApplicationContext());
        datasource.open();

        ASSETURL = this.getResources().getString(R.string.base_url) + this.getResources().getString(R.string.asset_url) + result;
        XAPIKEY = this.getResources().getString(R.string.api_key);
        UPDATEURL = this.getResources().getString(R.string.base_url) + this.getResources().getString(R.string.store_url) + result;

        /*Log.i("*** ASSET URL ***", ASSETURL);
        Log.i("*** API KEY ***", XAPIKEY);
        Log.i("*** UPDATE URL ***", UPDATEURL);*/

        try{

            JSONObject assetJSON = JSONParser.getJSONFromUrl(ASSETURL, XAPIKEY);
            //Log.i("*** ASSET JSON OBJECT ***", assetJSON.toString());
            JSONObject status = null;
            String success = null;

            try {
                // Getting Array of Login Validation
                status = assetJSON.getJSONObject(STATUS);
                success = status.getString("success");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(success.equals("Matches for query")){
                //Log.i("*** ASSET JSON ***", success);
                JSONArray dataArray = assetJSON.getJSONArray("data");
                //Log.i("**** DATAARRAY ****", dataArray.toString());
                JSONObject object = dataArray.getJSONObject(0);
                object.put("resource", "famaster");
                object.put("fama_dept", dpCode);
                object.put("fama_loc", lCode);
                object.put("fama_loc_memo", values[5]);
                object.put("fama_cond_cd", cCode);
                object.put("fama_serial", values[8]);
                object.put("fama_manuf", mCode);
                object.put("fama_model", values[10]);
                object.put("fama_model_yr", values[11]);
                object.put("fama_license", values[12]);
                object.put("fama_lst_inv_dt", values[16]);
                object.put("fama_ins_car", values[18]);
                object.put("fama_ins_val", values[19]);
                object.put("fama_ins_cost", values[21]);
                object.put("fama_ins_mem1", values[22]);
                object.put("fama_update_time", timeStr);

                //final JSONObject newObj = object;

                //Thread t = new Thread() {

                    //public void run() {
                        sendJson(object, UPDATEURL);
                    //}
                //};
                //t.start();

            } else {
                Log.i("*** ASSET JSON ***", "THIS NO WORKY!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        datasource.close();

    }

    protected void sendJson(final JSONObject jObj, final String url) {

        //Log.i("*** SENDJSON VALS ***", url);
        Thread t = new Thread() {

            public void run() {

                HttpClient client = new DefaultHttpClient();


                HttpPost post = new HttpPost(url);
                post.setHeader("X-API-KEY", XAPIKEY);
                JSONObject json = new JSONObject();
                json = jObj;
                String responseRes = null;
                InputStream in = null;
                //Log.i("*** SENDJSON ***", "POINT 1");

                try {

                    //HttpEntity entity = response.getEntity();
                    StringEntity entity = new StringEntity(json.toString());
                    //UrlEncodedFormEntity entity = new UrlEncodedFormEntity(pairs, HTTP.UTF_8);
                    entity.setContentType(new BasicHeader("Content-Type", "application/json"));
                    post.setEntity(entity);
                    HttpResponse response = client.execute(post);

                    in = entity.getContent();
                    //Log.i("*** SENDJSON ***", "POINT 2");


                } catch(Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (in != null)
                            in.close();
                    } catch (Exception squish) {
                    }
                }

            }
        };

        t.start();
        //Log.i("*** SENDJSON ***", "POINT 3");
    }

    private Runnable returnCVRes = new Runnable() {

        @Override
        public void run() {
            cv_ProgressDialog.dismiss();
        }
    };

    public void getNewCodes(){

        if(values[3].equals(" ") || values[3].equals("")){
            dpCode = " ";
        } else {
            MyDept newDept = datasource.getDeptCode(values[3]);
            dpCode = String.valueOf(newDept.getFadpCode());
        }

        if(values[4].equals(" ") || values[4].equals("")){
            lCode = " ";
        } else {
            MyLoc newLoc = datasource.getLocCode(values[4]);
            lCode = String.valueOf(newLoc.getLocCode());
        }

        if(values[7].equals(" ") || values[7].equals("")){
            cCode = " ";
        } else {
            MyCond newCond = datasource.getCondCode(values[7]);
            cCode = newCond.getCondCode();
        }

        if(values[9].equals(" ") || values[9].equals("")){
            mCode = " ";
            //Log.i("**** MCODE ****", values[9]);
        } else {
            MyManufac newManufac = datasource.getManufacCode(values[9]);
            mCode = newManufac.getManufacCode();
            //Log.i("**** MCODE ****", values[9]);
        }

        if(values[11].equals("0") || values[11].equals("No Entry")){
            values[11] = "0.00";
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        values[16] = sdf.format(c.getTime()) + " 00:00:00";

        SimpleDateFormat timeSdf = new SimpleDateFormat("HH:mm:ss");
        timeStr = timeSdf.format(c.getTime());

        if(values[19].equals("0")){
            values[19] = "0.00";
        }

        if(values[21].equals("0")){
            values[21] = "0.00";
        }

    }

}
