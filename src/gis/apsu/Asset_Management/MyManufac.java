package gis.apsu.Asset_Management;

/**
 * Created by dunnings on 12/12/13.
 */
public class MyManufac {

    private String manufacCode;
    private String manufacName;

    public MyManufac(String manufacCode, String manufacName) {
        this.manufacCode = manufacCode;
        this.manufacName = manufacName;
    }
    public MyManufac(){
    }
    @Override
    public String toString() {
        return manufacName;
    }

    public String getManufacCode() {
        return manufacCode;
    }

    public void setManufacCode(String manufacCode) {
        this.manufacCode = manufacCode;
    }

    public String getManufacDesc() {
        return manufacName;
    }

    public void setManufacDesc(String manufacName) {
        this.manufacName = manufacName;
    }
}