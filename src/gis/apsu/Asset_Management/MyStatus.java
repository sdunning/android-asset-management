package gis.apsu.Asset_Management;

/**
 * Created by dunnings on 12/12/13.
 */
public class MyStatus {

    private String statusCode;
    private String statusDesc;

    public MyStatus(String statusCode, String statusDesc) {
        this.statusCode = statusCode;
        this.statusDesc = statusDesc;
    }
    public MyStatus(){
    }
    @Override
    public String toString() {
        return statusDesc;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }


}
